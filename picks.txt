2018-09-01
2018-11-05
2018-09-02
2018-09-03
2018-09-04
2018-09-06
2018-09-07
2018-09-08
2018-09-09
2018-09-10
2018-09-11
2018-09-12
2018-09-13
2018-09-14
2018-09-15
2018-09-16
2018-09-17
2018-09-18
2018-09-19
2018-09-20
2018-09-21
2018-09-22
2018-09-23
2018-09-24
2018-09-25
2018-09-26
2018-09-27
2018-09-28
2018-09-29
2018-09-30
2018-10-01
2018-10-02
2018-10-03
2018-10-04
2018-10-05
2018-10-06
2018-10-07
2018-10-08
2018-10-09
2018-10-10
2018-10-11
2018-10-12
2018-10-13
2018-10-14
2018-10-16
2018-10-17
2018-10-18
2018-10-19
2018-10-20
2018-10-21
2018-10-22
2018-10-23
2018-10-24
2018-10-25
2018-10-26
2018-10-27
2018-10-28
2018-10-29
2018-10-30
2018-10-31
2018-11-01
2018-11-02
2018-11-03
2018-11-04
2018-11-05
These are the picks I have in my database: 

AwayTeam | Country | Date | Division | FTAG | FTHG | HomeTeam | Odds | Outcome | Prediction | Return | Type of Bet | Wager |  
 :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |  
 leeds | england | 02/10/18 | championship | 1.0 | 0.0 | hull | 1.5 | Win | dnb 2 | 0.75 | DNB | 0.5 |  
 middlesbrough | england | 02/10/18 | championship | 2.0 | 0.0 | ipswich | 1.42 | Win | dnb 2 | 0.71 | DNB | 0.5 |  
 inter | europe | 03/10/18 | champions league | 2.0 | 1.0 | psv | 2.25 | Push | dc x-1 | 0.5 | DC | 0.5 |  
 monaco | europe | 03/10/18 | champions league | 0.0 | 3.0 | dortmund | 1.82 | Win | 1&o2.5 | 0.91 | Over | 0.5 |  
 club brugge | europe | 03/10/18 | champions league | 1.0 | 3.0 | atletico madrid | 1.3 | Win | 1 | 1.3 | 1X2 | 1.0 |  
 galatasaray | europe | 03/10/18 | champions league | 0.0 | 1.0 | porto | 1.5 | Win | 1 | 1.5 | 1X2 | 1.0 |  
 millwall | england | 03/10/18 | championship | None | None | nottm forest | 1.25 | Push | dnb 1 | 2.0 | None | 2.0 |  
 rapid wien | europe | 04/10/18 | uefa europa league | 1.0 | 3.0 | rangers | 1.28 | Win | 1x | 0.64 | DC | 0.5 |  
 sevilla | europe | 04/10/18 | uefa europa league | 1.0 | 2.0 | fk krasnodar | 1.37 | Lost | x2 | 0.0 | DC | 0.5 |  
 arsenal | europe | 04/10/18 | uefa europa league | 3.0 | 0.0 | qarabag | 1.5 | Win | 2 | 0.75 | 1X2 | 0.5 |  
 slavia prague | europe | 04/10/18 | uefa europa league | 0.0 | 1.0 | zenit st petersburg | 1.9 | Lost | o2.5 | 0.0 | Over | 0.5 |  
 f91 dudelange | europe | 04/10/18 | uefa europa league | 0.0 | 3.0 | betis | 1.65 | Win | u3.5 | 0.825 | Under | 0.5 |  
 rb leipzig | europe | 04/10/18 | uefa europa league | 3.0 | 1.0 | rosenborg | 1.3 | Win | dnb 2 | 0.65 | DNB | 0.5 |  
 rapid wien | europe | 04/10/18 | uefa europa league | 1.0 | 3.0 | rangers | 1.45 | Win | dnb 1 | 0.725 | DNB | 0.5 |  
 genk | europe | 04/10/18 | uefa europa league | 1.0 | 3.0 | sarpsborg | 1.7 | Win | o2.5 | 0.85 | Over | 0.5 |  
 brentford | england | 05/10/18 | championship | 1.0 | 1.0 | leeds | 1.7 | Lost | o2.5 | 0.0 | Over | 0.25 |  
 rotherham | england | 05/10/18 | championship | 1.0 | 3.0 | birmingham | 1.65 | Win | 1 | 0.825 | 1X2 | 0.5 |  
 nottm forest | england | 05/10/18 | championship | None | None | middlesbrough | 1.8 | Push | 1 | 0.5 | None | 0.5 |  
 stoke | england | 05/10/18 | championship | 1.0 | 0.0 | norwich | 1.67 | Lost | dnb 1 | 0.0 | DNB | 0.5 |  
 aston villa | england | 05/10/18 | championship | 1.0 | 2.0 | millwall | 2.35 | Lost | o1.5 a | 0.0 | Over Team | 0.25 |  
 reading | england | 05/10/18 | championship | 1.0 | 4.0 | west bromwich | 1.68 | Win | o2.5 | 0.84 | Over | 0.5 |  
 arsenal | england | 07/10/18 | premier league | 5.0 | 1.0 | fulham | 1.9 | Win | 2&o2.5 | 3.8 | Over | 2.0 |  
 feyenoord | netherlands | 07/10/18 | eredivisie | 1.0 | 1.0 | willem ii | 1.54 | Lost | 2 | 0.0 | 1X2 | 2.0 |  
 chelsea | england | 07/10/18 | premier league | 3.0 | 0.0 | southampton | 2.47 | Win | 2&o2.5 | 1.8525 | Over | 0.75 |  
 man city | england | 07/10/18 | premier league | 0.0 | 0.0 | liverpool | 1.8 | Lost | btts&o2.5 | 0.0 | Over | 0.75 |  
 feyenoord | netherlands | 07/10/18 | eredivisie | 1.0 | 1.0 | willem ii | 1.61 | Push | btts | 2.0 | BTTS | 2.0 |  
 dinamo bucharest | romania | 08/10/18 | liga 1 | 0 | 2 | botosani | 1.55 | Lost | x2 | 0.0 | DC | 0.5 |  
 duisburg | germany | 08/10/18 | 2. bundesliga | 2 | 1 | fc koln | 1.83 | Win | 1&o2.5 | 1.3725 | Over | 0.75 |  
 metz | france | 08/10/18 | ligue ii | 2 | 1 | sochaux | 1.35 | Win | x2 | 1.35 | DC | 1.0 |  
 swansea | england | 20/10/18 | championship | 0.0 | 1.0 | aston villa | 1.9 | None | 1 | 0.95 | 1X2 | 0.5 |  
 bristol city | england | 20/10/18 | championship | 1.0 | 0.0 | brentford | 1.57 | Push | dc x-1 | 0.33 | DC | 0.33 |  
 norwich | england | 20/10/18 | championship | None | None | nottm forest | 1.85 | Push | u2.5 | 0.33 | None | 0.33 |  
 birmingham | england | 20/10/18 | championship | 1.0 | 0.0 | stoke | 2.42 | Push | dc x-2 | 0.25 | DC | 0.25 |  
 watford | england | 20/10/18 | premier league | 2.0 | 0.0 | wolves | 1.26 | Lost | dnb 1 | 0.0 | DNB | 0.75 |  
 emmen | netherlands | 20/10/18 | eredivisie | 0.0 | 6.0 | psv | 1.3 | None | 1&o2.5 | 1.3 | Over | 1 |  
 as monaco | france | 20/10/18 | ligue 1 | 1.0 | 2.0 | strasbourg | 1.65 | None | btts yes | 0.825 | BTTS | 0.5 |  
 west bromwich | england | 20/10/18 | championship | 0.0 | 1.0 | wigan | 1.61 | Lost | dnb 2 | 0.0 | DNB | 0.4 |  
 birmingham | england | 20/10/18 | championship | 1.0 | 0.0 | stoke | 1.81 | None | x2 | 0.7240000000000001 | DC | 0.4 |  
 derry city | ireland | 22/10/18 | premier division | 1 | 2 | bray | 1.75 | Win | u3.5 | 1.75 | Under | 1.0 |  
 bohemians | ireland | 22/10/18 | premier division | 1 | 1 | sligo rovers | 2.05 | Win | u2.5 | 2.05 | Under | 1.0 |  
 juventus | europe | 23/10/18 | champion's league | 1.0 | 0.0 | man utd | 1.4 | Win | x2 | 0.462 | DC | 0.33 |  
 lyon | europe | 23/10/18 | champion's league | 3.0 | 3.0 | hoffenheim | 1.41 | Win | 1x | 0.4653 | DC | 0.33 |  
 benfica | europe | 23/10/18 | champion's league | 0.0 | 1.0 | ajax | 3.22 | Lost | 1&o2.5 | 0.0 | Over | 0.33 |  
 norwich | england | 23/10/18 | championship | None | None | aston villa | 1.7 | Push | btts yes | 0.33 | None | 0.33 |  
 reading | england | 23/10/18 | championship | 1.0 | 2.0 | birmingham | 1.6 | Win | o0.5 a | 0.528 | Over Team | 0.33 |  
 blackburn | england | 23/10/18 | championship | 1.0 | 3.0 | swansea | 1.3 | Win | o1.5 | 0.429 | Over | 0.33 |  
 aston villa | england | 23/10/18 | championship | 1.0 | 2.0 | norwich | 1.7 | Win | btts yes | 0.561 | BTTS | 0.33 |  
 blackburn | england | 23/10/18 | championship | 1.0 | 3.0 | swansea | 1.74 | Win | btts yes | 0.5742 | BTTS | 0.33 |  
 tottenham | europe | 25/10/18 | champion's league | 2.0 | 2.0 | psv | 1.73 | Win | 1x | 0.5709 | DC | 0.33 |  
 monaco | europe | 25/10/18 | champion's league | 1.0 | 1.0 | club brugge | 1.28 | Win | 1x | 0.4224 | DC | 0.33 |  
 schalke 04 | europe | 25/10/18 | champion's league | 0.0 | 0.0 | galatasaray | 1.84 | Push | dnb 1 | 0.33 | DNB | 0.33 |  
 hull | england | 25/10/18 | championship | 0.0 | 1.0 | bristol city | 1.33 | Win | 1 | 0.4389 | 1X2 | 0.33 |  
 derby | england | 25/10/18 | championship | 4.0 | 1.0 | west brom | 1.7 | Win | btts yes | 0.561 | BTTS | 0.33 |  
 olympiakos | europe | 25/10/18 | champion's league | 2.0 | 0.0 | f91 dudelange | 1.35 | Win | u3.5 | 0.4455 | Under | 0.33 |  
 dinamo zagreb | europe | 25/10/18 | champion's league | None | None | spartak trnava | 2.37 | Push | btts&o2.5 | 0.33 | None | 0.33 |  
 fenerbahce | europe | 25/10/18 | champion's league | 2.0 | 2.0 | anderlecht | 1.32 | Lost | u3.5 | 0.0 | Under | 0.33 |  
 dinamo zagreb | europe | 25/10/18 | champion's league | None | None | spartak trnava | 3.0 | Push | (eh -1) 2 | 0.25 | None | 0.25 |  
 spartak moscow | europe | 25/10/18 | champion's league | 0.0 | 0.0 | rangers | 2.2 | Lost | 1 | 0.0 | 1X2 | 1.0 |  
 apollon limassol | europe | 25/10/18 | champion's league | 0.0 | 2.0 | eintracht frankfurt | 2.04 | Lost | btts&o2.5 | 0.0 | Over | 0.5 |  
 olympiakos | europe | 25/10/18 | europa league | 2.0 | 0.0 | f91 dudelange | 1.35 | Win | u3.5 | 0.4455 | Under | 0.33 |  
 dinamo zagreb | europe | 25/10/18 | europa league | None | None | spartak trnava | 2.37 | Push | btts&o2.5 | 0.33 | None | 0.33 |  
 fenerbahce | europe | 25/10/18 | europa league | 2.0 | 2.0 | anderlecht | 1.32 | Lost | u3.5 | 0.0 | Under | 0.33 |  
 dinamo zagreb | europe | 25/10/18 | europa league | None | None | spartak trnava | 3.0 | Push | (eh -1) 2 | 0.25 | None | 0.25 |  
 spartak moscow | europe | 25/10/18 | europa league | 0.0 | 0.0 | rangers | 2.2 | Lost | 1 | 0.0 | 1X2 | 1.0 |  
 apollon limassol | europe | 25/10/18 | europa league | 0.0 | 2.0 | eintracht frankfurt | 2.04 | Lost | btts&o2.5 | 0.0 | Over | 0.5 |  
 spartak moscow | europe | 25/10/18 | europa league | 0.0 | 0.0 | rangers | 1.52 | Push | dnb 1 | 1.0 | DNB | 1.0 |  
 derby | england | 27/10/18 | championship | 1.0 | 1.0 | middlesbrough | 2.5 | Push | dnb 2 | 0.33 | DNB | 0.33 |  
 sheffield wed | england | 27/10/18 | championship | 1.0 | 3.0 | birmingham | 1.8 | Win | 1 | 0.594 | 1X2 | 0.33 |  
 brentford | england | 27/10/18 | championship | 0.0 | 1.0 | norwich | 1.9 | Win | dc x1 | 0.627 | DC | 0.33 |  
 rotherham | england | 27/10/18 | championship | 1.0 | 1.0 | preston | 1.65 | Lost | 1 | 0.0 | 1X2 | 0.33 |  
 blackburn | england | 27/10/18 | championship | 1.0 | 1.0 | west brom | 2.8 | Lost | 1&o2.5 | 0.0 | Over | 0.33 |  
 nottm forest | england | 27/10/18 | championship | None | None | leeds | 3.65 | Push | x | 0.25 | None | 0.25 |  
 bournemouth | england | 27/10/18 | premier league | 3.0 | 0.0 | fulham | 1.9 | Win | x2 | 0.627 | DC | 0.33 |  
 strasbourg | france | 27/10/18 | ligue 1 | 1.0 | 1.0 | guingamp | 1.6 | Win | x2 | 0.528 | DC | 0.33 |  
 sheff wed | england | 27/10/18 | championship | 1.0 | 3.0 | birmingham | 1.85 | Win | 1 | 0.6105 | 1X2 | 0.33 |  
 schalke | germany | 28/10/18 | bundesliga | 0.0 | 0.0 | rb leipzig | 1.87 | Lost | 1 | 0.0 | 1X2 | 0.33 |  
 leverkusen | germany | 28/10/18 | bundesliga | 6.0 | 2.0 | werder bremen | 1.65 | Lost | dnb 1 | 0.0 | DNB | 0.33 |  
 arsenal | england | 28/10/18 | premier league | 2.0 | 2.0 | c palace | 1.77 | Lost | 2 | 0.0 | 1X2 | 0.33 |  
 feyenoord | netherlands | 28/10/18 | eredivise | 0.0 | 3.0 | ajax | 1.5 | Lost | btts yes | 0.0 | BTTS | 0.33 |  
 heerenveen | netherlands | 28/10/18 | eredivise | 3.0 | 2.0 | az alkmaar | 1.6 | Win | btts yes | 0.528 | BTTS | 0.33 |  
 

2018-10-01
2018-10-02
2018-10-03
2018-10-04
2018-10-05
2018-10-06
2018-10-07
2018-10-08
2018-10-09
2018-10-10
2018-10-11
2018-10-12
2018-10-13
2018-10-14
2018-10-15
2018-10-16
2018-10-17
2018-10-18
2018-10-19
2018-10-20
2018-10-21
2018-10-22
2018-10-23
2018-10-24
2018-10-25
2018-10-26
2018-10-27
2018-10-28
2018-10-29
2018-10-30
2018-10-31
Tipster Bot October report: Week 4
In order to get some reliable information on the tipsters in the Daily Picks Thread from /r/SoccerBetting, this Bot tracks the picks and     writes consecutive weekly reports. At the beginning of each month they start anew. These are the results from **01/10/18 to the 31/10/18**.

Since the beginning of October, we had 22 Tipsters and 271 Tips that were tracked by the bot. The results are presented in the table below.

The **Tipster of the Month** is **/u/thrrowaway55**  with a ROI of 61.56% over 9 Picks. Congratulations!     The Tipsters are sorted after the highest product of ROI and Number of Tips (i.e. the Tipster that had the highest wins, if all tipsters had the same stake per game).


Placing | Tipster | Stakes [Units] | Wins [Units] | Tips | Prediction Rate [%] | ROI [%] | Mean Odds | Main Betting Style | 
:-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | 
**1.** | /u/thrrowaway55 | 9.0 | 5.540 | 9 | 88.89% | 61.56% | 2.004 | 1X2 | 
**2.** | /u/Thijs1512 | 5.9 | 3.600 | 4 | 25.00% | 61.02% | 1.735 | BTTS | 
**3.** | /u/coolcoconut123 | 9.0 | 4.360 | 5 | 80.00% | 48.44% | 1.892 | BTTS | 
4. | /u/Hitlers_Art_Teacher | 3.5 | 3.455 | 2 | 100.00% | 98.71% | 1.995 | Over | 
5. | /u/ExperientialAra | 325.0 | 7.790 | 80 | 65.00% | 2.40% | 1.589 | 1X2 | 
6. | /u/peruvianhope | 8.0 | 1.480 | 8 | 62.50% | 18.50% | 2.296 | 1X2 | 
7. | /u/j21w91 | 4.0 | 1.060 | 4 | 50.00% | 26.50% | 2.232 | 1X2 | 
8. | /u/PL-BT | 5.0 | 4.500 | 1 | 100.00% | 90.00% | 1.900 | 1X2 | 
9. | /u/eagles490 | 4.09 | 0.669 | 3 | 66.67% | 16.34% | 1.733 | 1X2 | 
10. | /u/Wolfframm | 20.0 | -1.010 | 5 | 60.00% | -5.05% | 1.602 | AH | 
11. | /u/neonzzzzz | 2.0 | -2.000 | 1 | 0.00% | -100.00% | 1.610 | Over | 
12. | /u/blackandrose56 | 2.0 | -2.000 | 1 | 0.00% | -100.00% | 1.440 | 1X2 | 
13. | /u/PM_ME_UR_WORRIMENTS | 35.06 | -0.817 | 65 | 60.00% | -2.33% | 1.686 | 1X2 | 
14. | /u/thatguyQ1 | 3.0 | -1.570 | 3 | 33.33% | -52.33% | 1.713 | 1X2 | 
15. | /u/tropianhs | 80.0 | -18.960 | 9 | 33.33% | -23.70% | 2.701 | 1X2 | 
16. | /u/trunutz | 33.6 | -4.910 | 16 | 37.50% | -14.61% | 2.841 | 1X2 | 
17. | /u/TheBigBossD | 5.0 | -3.190 | 4 | 25.00% | -63.80% | 1.715 | 1X2 | 
18. | /u/VitorinoBJ | 3.0 | -3.000 | 3 | 0.00% | -100.00% | 2.070 | Over | 
19. | /u/HoodieBae | 5.0 | -5.000 | 3 | 0.00% | -100.00% | 2.310 | DC | 
20. | /u/GonnSoares | 11.0 | -3.980 | 13 | 38.46% | -36.18% | 1.898 | Under | 
21. | /u/takeall3 | 7.0 | -5.020 | 7 | 14.29% | -71.71% | 2.214 | 1X2 | 
22. | /u/poisonfoot | 8.28 | -2.410 | 25 | 28.00% | -29.11% | 2.176 | AH | 
Thanks to all participants. Please take into account that the bot does not track all tipsters. If you are interested in having your tips logged next week, I made an in-depth [post](https://www.reddit.com/user/tropianhs/comments/7o4o4o/how_do_i_get_tracked_by_the_tipster_bot/) how to format your tips in the Daily Picks Thread.

Until next week, good luck to all and happy betting. 




---

^(This post was created automatically by a scripting bot. If you find a mistake, just PM me. If you would like to add a feature to the bot, we can discuss it in the comments.)



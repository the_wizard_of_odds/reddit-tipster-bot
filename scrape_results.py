import datetime
import os

from Functions.Scraping.scraping import scrape_results_from_betexplorer
from Functions.Database.database import write_results_to_database 

dir_path = os.path.dirname(os.path.realpath(__file__))


#for i in range(30):
date = datetime.date.today()-datetime.timedelta(days=1)
print(date)
#date = datetime.date.today()-datetime.timedelta(days=7)

results = scrape_results_from_betexplorer(date)

write_results_to_database(results, date, dir_path)

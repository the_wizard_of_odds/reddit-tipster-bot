
from Functions.Database.database import write_picks_to_database,evaluate_picks,tipster_report

import datetime 

beginning = datetime.date.today()-datetime.timedelta(days=1)
ending = datetime.date.today()-datetime.timedelta(days=1)

evaluate_picks(beginning,ending)

tipsters = tipster_report(beginning,ending)

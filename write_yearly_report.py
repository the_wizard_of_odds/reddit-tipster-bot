from Functions.Database.database import write_picks_to_database,evaluate_picks,tipster_report
from Functions.Reddit.reddit import write_in_depth_tipster

import datetime 
import os
import numpy as np
import pylab as pl

dir_path = os.path.dirname(os.path.realpath(__file__))

#begining = datetime.date.today()-datetime.timedelta(days=7)
#begining = datetime.date.today().replace(day=1)
#ending = datetime.date.today()-datetime.timedelta(days=1)
begining = datetime.date(2018,6,1)
ending = datetime.date(2018,12,31)

evaluate_picks(begining,ending,dir_path)

tipsters = tipster_report(begining,ending,dir_path)

write_in_depth_tipster(tipsters,begining,ending)

 

    
    

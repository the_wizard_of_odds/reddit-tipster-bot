# Reddit Tipster BOT

This is a BOT that analyses the tipsters that post their tip on SoccerBetting subreddit and evaluates tipsters profitability over time.  

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

To be able to run the software you need a `praw.ini` file. For help on how to register a reddit BOT look here: http://progur.com/2016/09/how-to-create-reddit-bot-using-praw4.html

The final `praw.ini` file will look something like this.

```
[tipster_bot]
client_id=<your registered bot id>
client_secret=<your registered bot seret>
password=<your reddit password>
username=<your reddit username>
```

### Installing

We recommend creating a virtualenv and install the requirements there. 
Here are the steps to follow. 
 
```
virtualenv venv
source venvtest/bin/activate
pip install requirements.txt
```

Once the requirements have been installed you should be able to run:
```
python get_tips_from_reddit.py
```

This will output a csv file in the Database directory with the tips published for the day. 


## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc


import numpy as np

def place_bet(home_goals, away_goals, betted_odds, prediction, stake):
    """place_bet(game, prediction, invested_money):
    
        make a bet with given object of class game, a prediction and invested money
        returns your wins (obviously 0 if you predict wrong)
        
        """

    # obviously if the prediction is correct, you get money*odds
    # if not http://giphy.com/gifs/nYogYgSmIJaIo

    home_goals = np.float(home_goals)
    away_goals = np.float(away_goals)
    betted_odds = np.float(betted_odds)
    
    prediction = str(prediction)
    prediction = prediction.replace(',','.')
    
    push = True
    won = False
    half_won = False
    half_lose = False
    betting_type = None
    
    if prediction.lower() in ['h','d','a'] or prediction.lower() in ['1','x','2']:
        betting_type = '1X2'
        
        prediction = prediction.replace('h','1').replace('d','x').replace('a','2')
        
    if 'BTTS'.lower() in prediction.lower()  or 'BTS'.lower() in prediction.lower():
        betting_type = 'BTTS' 
    
    if 'over'.lower() in prediction.lower()  or ('o'.lower() in prediction.lower() and 'no' not in prediction.lower())or 'ovr'.lower() in prediction.lower():
        if 'h' in prediction.lower() or 'a' in prediction.lower():
            betting_type = 'Over Team'
        else:    
            betting_type = 'Over'
        
        prediction = prediction.lower().replace('ver','').replace('vr','').replace(' ','')
        
    if 'under'.lower() in prediction.lower()  or 'u'.lower() in prediction.lower() or 'undr'.lower() in prediction.lower():
        if 'h' in prediction.lower() or 'a' in prediction.lower():
            betting_type = 'Under Team'
        else:    
            betting_type = 'Under'   
        
        prediction = prediction.lower().replace('nder','').replace('ndr','').replace('dr','').replace(' ','')
        
    if 'ah'.lower() in prediction.lower():
        betting_type = 'AH'   
        
        prediction = prediction.replace(' ','')

    if 'eh'.lower() in prediction.lower():
        betting_type = 'EH'   
        
        prediction = prediction.replace(' ','')
        
    if 'DNB'.lower() in prediction.lower():
        betting_type = 'DNB'    
        
        
    if 'DC'.lower() in prediction.lower() or prediction.lower() in ['1x','2x','12','x1','x2','21'] or prediction.lower() in ['hd','ha','da']:
        betting_type = 'DC'       
        
        if prediction.lower() in ['1x','2x','12','x1','x2','21'] or prediction.lower() in ['hd','ha','da']:
            prediction = 'dc ' + prediction.replace('h','1').replace('d','x').replace('a','2')

#    print prediction,betting_type

    if betting_type == 'EH':
        
        second_argument = prediction[2]
        third_argument = np.float(prediction[3::])

        if second_argument == '1' or second_argument.lower() == 'h':
                if home_goals+third_argument > away_goals:
                    won =True
                    push = False

                if home_goals+third_argument <= away_goals:
                    won =False
                    push = False  
                    
        if second_argument == '2' or second_argument.lower() == 'a':
        	if home_goals < away_goals + third_argument:
            		won =True
            		push = False
        
        	if home_goals >= away_goals + third_argument:
            		won =False
            		push = False

        if second_argument == 'x' or second_argument.lower() == 'd':
            if third_argument > 0:
                if home_goals == away_goals + third_argument:
                    won =True
                    push = False
                else:
                    won =False
                    push = False

        if third_argument < 0:
            if home_goals+third_argument == away_goals:
                won =True
                push = False
            else:
                won =False
                push = False
    
    if betting_type == 'AH':
        
        second_argument = prediction[2]
        third_argument = np.float(prediction[3::])
        #print(third_argument)
        handicap = None
        
#       print  third_argument,third_argument - np.int(third_argument),abs(third_argument - np.int(third_argument)) <0.3 and abs(third_argument - np.int(third_argument)) <0.2
        
        if abs(third_argument - np.int(third_argument)) <0.1:
            handicap = 'full'
            
        if abs(third_argument - np.int(third_argument)) <0.6 and abs(third_argument - np.int(third_argument)) >0.4:
            handicap = 'half' 
            
        if abs(third_argument - np.int(third_argument)) <0.3 and abs(third_argument - np.int(third_argument)) >0.2:
            handicap = 'quarter' 
            
        if abs(third_argument - np.int(third_argument)) <0.8 and abs(third_argument - np.int(third_argument)) >0.7:
            handicap = 'quarter'    
            
        if handicap == 'quarter':
            push = False

            if second_argument == '1' or second_argument.lower() == 'h':
                
                calc_hand = home_goals + third_argument - away_goals
                
                if calc_hand > 0.25:
                    won =True
                    
                elif calc_hand == 0.25:
                    half_won = True  

                elif calc_hand == -0.25:
                    half_lose = True
                else:
                    won = False

            if second_argument == '2' or second_argument.lower() == 'a':
                calc_hand = away_goals + third_argument - home_goals
                
                if calc_hand > 0.25:
                    won =True
                    
                elif calc_hand == 0.25:
                    half_won = True  

                elif calc_hand == -0.25:
                    half_lose = True
                else:
                    won = False
#            print "Here"
            
#        print handicap    
            
        if handicap == 'half':
            
            if second_argument == '1' or second_argument.lower() == 'h':
                if home_goals+third_argument > away_goals:
                    won =True
                    push = False
                    
                if home_goals+third_argument < away_goals:
                    won =False
                    push = False  
                    
            if second_argument == '2' or second_argument.lower() == 'a':
                if home_goals < away_goals +third_argument:
                    won =True
                    push = False
                
                if home_goals > away_goals +third_argument:
                    won =False
                    push = False 
            
            
        if handicap == 'full':
            
            if second_argument == '1' or second_argument.lower() == 'h':
                if home_goals+third_argument > away_goals:
                    won =True
                    push = False
                    
                if home_goals+third_argument == away_goals:
                    won =False
                    push = True    
                
                if home_goals+third_argument < away_goals:
                    won =False
                    push = False  
                    
            if second_argument == '2' or second_argument.lower() == 'a':
                if home_goals < away_goals +third_argument:
                    won =True
                    push = False
                    
                if home_goals == away_goals +third_argument:
                    won =False
                    push = True    
                
                if home_goals > away_goals +third_argument:
                    won =False
                    push = False        
    
    if betting_type == '1X2':
        if str(prediction) == '1':
            if home_goals>away_goals:
                won = True
                push = False
            else:
                won = False
                push = False
        
        if str(prediction) == '2':
            if home_goals<away_goals:
                won = True
                push = False
            else:
                won = False 
                push = False
                
        if str(prediction).lower() == 'x':
            if home_goals==away_goals:
                won = True
                push = False
            else:
                won = False 
                push = False
            
    if betting_type == 'BTTS':      

        second_argument = str(prediction.lower().split('btts')[1].strip())

        if second_argument.lower() in (['yes','y','true']):
            second_argument = True     
        elif second_argument.lower() in (['no','n','false']):
            second_argument = False    
            
        
        if second_argument == True:        
            if home_goals>0 and away_goals>0:
                won = True
                push = False
            else:
                won = False
                push = False
                
        if second_argument == False:        
            if home_goals<1 or away_goals<1:
                won = True
                push = False
            else:
                won = False 
                push = False
                
    if betting_type == 'Over':
        
        second_argument = prediction.lower().split('o')[1].strip()
        #print prediction
        if np.float(home_goals+away_goals) > np.float(second_argument):
            won = True
            push = False
        else:
            won = False
            push = False
            
    if betting_type == 'Over Team':
        
#        second_argument = prediction.lower().split('o')[1].strip().split(' ')[1]
        
        if 'h' in prediction.lower():
            goals = np.float(home_goals)
        if 'a' in prediction.lower():
            goals = np.float(away_goals)
            
        second_argument = prediction.lower().replace('h','').replace('a','').split('o')[1].strip()            
#        print prediction     
        if goals > np.float(second_argument):
            won = True
            push = False
        else:
            won = False
            push = False  
            
    if betting_type == 'Under Team':
        
        second_argument = prediction.lower().replace('h','').replace('a','').split('u')[1].strip()
        #print prediction
        if 'h' in prediction.lower():
            goals = np.float(home_goals)
        if 'a' in prediction.lower():
            goals = np.float(away_goals)
        if goals < np.float(second_argument):
            won = True
            push = False
        else:
            won = False
            push = False        
            
    if betting_type == 'Under':
        
        second_argument = prediction.lower().split('u')[1].strip()
        
        if np.float(home_goals+away_goals) < np.float(second_argument):
            won = True
            push = False
        else:
            won = False  
            push = False
            
    if betting_type == 'DC':
        
        second_argument = prediction.lower().split('dc')[1].strip()
        
              
        if second_argument.lower() == '1x' or second_argument.lower() == 'x1':
            if home_goals>=away_goals:
                won = True
                push = False
            else:
                won = False  
                push = False
            
        if second_argument.lower() == '2x' or second_argument.lower() == 'x2':
            if home_goals<=away_goals:
                won = True
                push = False
            else:
                won = False 
                push = False
            
        if second_argument.lower() == '12' or second_argument.lower() == '21':
            if home_goals != away_goals:
                won = True
                push = False
            else:
                won = False 
                push = False
            
    if betting_type == 'DNB':
        
        second_argument = prediction.lower().split('dnb')[1].strip()
        
        if second_argument.lower() == '1' or second_argument.lower() == 'h':
            if home_goals>away_goals:
                won = True
                push = False
            else:
                won = False
                push = False
            
        if second_argument.lower() == '2' or second_argument.lower() == 'a':
            if home_goals<away_goals:
                won = True
                push = False
            else:
                won = False  
                push = False
                
        if home_goals == away_goals:
                push = True
                won = False                
                
                 
    if won:
        wins = np.float(betted_odds)*np.float(stake)
    elif half_won:
        wins = np.float(betted_odds)*np.float(stake)/2.
    elif half_lose:
        wins = np.float(stake)/2.
    elif push:
        wins = np.float(stake)
    else:
        wins = 0.0
        
#    if won == True:
#        print "Won"
#    else:
#        print "Lost"

    return wins, betting_type


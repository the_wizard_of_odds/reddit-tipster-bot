from lxml import html
import requests
import numpy as np
import pandas as pd




def betexplorer_get_html_from_fixtures_page(date):
    
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:12.0)' } #you somehow have to identify the browser or its not answering

    parameters = (('year', date.isoformat().split('-')[0]), ('month',date.isoformat().split('-')[1]),('day', date.isoformat().split('-')[2]))
         
    page_url = 'http://www.betexplorer.com/results/soccer/'
    
    page = requests.get(page_url,params=parameters,headers=headers)
    print(page.url)
    
    tree = html.fromstring(page.content)
    
    return tree,page.url 
    
def scrape_results_from_betexplorer(date):
    
    outcome_header = ['Country','Division','Date','HomeTeam','AwayTeam','FTHG','FTAG']
    data = pd.DataFrame(columns=outcome_header)
    
    tree, url =betexplorer_get_html_from_fixtures_page(date)

    table = tree.xpath('//tr')
    
    game_number =0
    #Iterates through all games and extracts the data that we are interested in 
    for i in range(len(table)-1):
    
        if table[i].get('class') =="js-tournament":
            country = table[i][0].text_content().split(':')[0]
            division = table[i][0].text_content().split(':')[1]
            
    
        # If this is not given it is not a game but a table-title or something else
        #there is no else because otherwise we can discard the data
#        if len(table[i][0].items())==1  and int(table[i].get('data-dt').split(',')[0]) == date.day:    
        if len(table[i][0].items())==1:
            
#            print(table[i].getchildren()[2][0].text_content())
            
            entries = table[i].getchildren()

            try:
                outcome = entries[1][0].text_content()
            except:
                try:
                    outcome = entries[2][0].text_content()
                except:
                    outcome = 'CAN.' 
            
            if outcome == '':
                outcome ='CAN.'

            #print(outcome)    
            if 'Live' not in outcome:    
    
                #This is only true if the game actually happened, otherwise we also don't need the data 
                #This means stuff like POSTP. for Postponed, ABN. for Abandoned and various other stuff that can happen        
                if not(any(x in outcome for x in ['POSTP.', 'CAN.', 'AWA.','ABN.','WO.'])):
                
                    data.set_value(game_number,["Country"],country.lower())
                    data.set_value(game_number,["Division"],division.lower())
                    #TODO Section below this: Horrible, this needs to be parsed by keywords not by stuff like this entries[0].getchildren()[0]
                    
                    #The team names are hidden in the string "Home Team Name - Away Team Name". I.e. splitting it at ' - ' returns both names
                    data.set_value(game_number,["HomeTeam"],entries[0].getchildren()[1].text_content().split(' - ')[0].strip().lower())
                    data.set_value(game_number,["AwayTeam"],entries[0].getchildren()[1].text_content().split(' - ')[1].strip().lower())
                    #print(entries[0].getchildren()[1].text_content().split(' - ')[1].strip().lower())
                
                    data.set_value(game_number,["FTHG"],np.int(outcome.split(':')[0]))
                    
                    # might contain additonal strings like "2:0 ABN."
                    data.set_value(game_number,["FTAG"],np.int(outcome.split(':')[1].split(' ')[0]))
                    
                    data.set_value(game_number,["Date"],date.strftime("%d/%m/%y"))
                    
                    #increase game_number
                    game_number += 1
    #print(data[data.AwayTeam=="shrewsbury"])   
    #print(list(data.AwayTeam))         
    return data

    
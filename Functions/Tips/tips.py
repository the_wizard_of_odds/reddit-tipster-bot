import pandas as pd
from lxml import html 
import datetime
import pytz
import string 
import numpy as np
#print tz

from Functions.Database.database import similarity

printable = set(string.printable)

tip_header = ['Author','Country','Division','Date','HomeTeam','AwayTeam','Prediction','Odds','Wager','Post Time']
tip_header_reduced = tip_header
tip_header_reduced.remove('Author')
tip_header_reduced.remove('Date')
tip_header_reduced.remove('Post Time')

def generate_empty_tip_dataframe(header):
    
    dataframe = pd.DataFrame(columns=header)
    
    return dataframe
    
def merge_tips_to_dataframe(list_of_tips):

    df = pd.concat(list_of_tips)

    return df    

     

def extract_tips_from_comment(comment):
    
    tips = None
    author = None
    tips_full = []
    try:
        author = comment.author.name
        valid_comment = True
    except:
        valid_comment = False
        
    if valid_comment:    
        
        date = datetime.datetime.fromtimestamp(comment.created)
        
        tree = html.fromstring(comment.body_html)
        
        try:
            list_tables = tree.xpath('//table')
        except:
            return author,tips
            
        for tab in range(len(tree.xpath('//table'))):
            
            table = list_tables[tab]
            
        
            header_table = table[0]
            header = []
            
            
            for i in range(len(header_table[0])):
                if header_table[0][i].text is not None:
                    header.append(header_table[0][i].text)
                else:
                    try:
                        header.append(header_table[0][i][0].text)
                    except:
                        pass
                
    #        print header   
                
            header_in_comment = []    
            for i in range(len(tip_header_reduced)):   
                #print author,comment.body
                scores =[]
                
                for j in range(len(header)):  #
                    if header[j] is not None:
                        scores.append(similarity(tip_header_reduced[i],header[j])>0.8)
                    else:
                        scores.append(False)
                
                if True in scores:
                    header_in_comment.append(True)
                else:
                    header_in_comment.append(False)

                
            if False not in header_in_comment:
#                print author
#                print "Body:",comment.body
#                print header, header_in_comment
                
                tips =generate_empty_tip_dataframe(tip_header)
        
                #for all rows
                for j in range(len(table[1])):
                    
                    #each column entry
                    for i in range(len(table[1][j])):
                        scores = []
                        for k in range(len(tip_header_reduced)):  
                            scores.append(similarity(tip_header_reduced[k],header[i]))
                            
                        if np.max(scores)>0.74:
                            if table[1][j][i].text is not None:
                                tips.set_value(j,tip_header_reduced[np.argmax(scores)],str(filter(lambda x: x in printable,table[1][j][i].text.lower().replace(',','.'))))

                    
                    
                    tips.set_value(j,"Author",author)
                    tips.set_value(j,"Date",date.strftime("%d/%m/%y"))
                    tips.set_value(j,"Post Time",datetime.datetime.fromtimestamp(comment.created_utc))
                    tips.set_value(j,"Fetched at",datetime.datetime.utcnow())
            
                tips_full.append(tips)       
    
    if len(tips_full)>0:
        tips = pd.concat(tips_full)
    
    return author,tips

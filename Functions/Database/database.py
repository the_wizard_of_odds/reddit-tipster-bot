import pandas as pd
import os.path
import datetime
import numpy as np

from difflib import SequenceMatcher

from scipy.stats import t

from Functions.Betting.place_bet import place_bet

def get_dataframe_from_database(filename):
    
    if os.path.isfile(filename) :
        df = pd.read_csv(filename)
        df = df.where((pd.notnull(df)), None)
    else:
        print("Not a valid file name")
    
    return df
    
def similarity(a, b):
    return SequenceMatcher(None, a, b).ratio()    

def write_picks_to_database(tips, date, dir_path):
    
    filename = dir_path + '/Database/'+ date.strftime("%d_%m_%Y")+'.csv'  
    
    if os.path.isfile(filename):
        tips_database = get_dataframe_from_database(filename)
        
        tips = pd.concat([tips_database,tips])
        
    tips["Prediction"] = tips["Prediction"].astype('str')
    tips["Wager"] = tips["Wager"].replace('u','').replace('U','')
    
    if 'FTHG' in list(tips):
        tips = tips.drop_duplicates(['Author','Country','Division','Date','HomeTeam','AwayTeam','Prediction'],keep='last')
    else:
        tips = tips.drop_duplicates(['Author','Country','Division','Date','HomeTeam','AwayTeam','Prediction'],keep='first')

#    print tips
#    if 'FTHG' in tips.keys():
#        tips = tips.drop_duplicates(tips.columns.difference(['Odds','Wager','FTHG','FTAG']))
#    else:
#        tips = tips.drop_duplicates(tips.columns.difference())
    
    
    tips.index = range(len(tips))
    
    tips.to_csv(filename, header=True, index=False)
    
    return tips    
    
def write_results_to_database(results, date, dir_path):
    
    filename = dir_path + '/Database/'+ date.strftime("%d_%m_%Y")+'.csv'
    
    if os.path.isfile(filename):
        tips = get_dataframe_from_database(filename)  
        
        tips["FTHG"] = None
        tips["FTAG"] = None        

        for i in range(len(tips)):
            
            #this makes the search faster
            reduced_results = results.loc[results['Country'] == tips["Country"][i]]    
            reduced_results.index = range(len(reduced_results))
            
            for j in range(len(reduced_results)):
                if similarity(tips["HomeTeam"][i],reduced_results["HomeTeam"][j])>0.6 \
                and similarity(tips["AwayTeam"][i],reduced_results["AwayTeam"][j])>0.6\
                and ('u19' in tips["HomeTeam"][i]) == ('u19' in reduced_results["HomeTeam"][j])\
                and ('u19' in tips["AwayTeam"][i]) == ('u19' in reduced_results["AwayTeam"][j]):
   
                    tips.set_value(i,"FTHG",reduced_results["FTHG"][j])
                    tips.set_value(i,"FTAG",reduced_results["FTAG"][j])    

                    
        #print(tips[tips["AwayTeam"]=='shrewsbury'])            
        write_picks_to_database(tips, date, dir_path)
        
def get_tips_by_name(beginning_date,end_date,name,last=None):   

    date = beginning_date
    tips = None
    while date <= end_date:
        
        filename = 'Database/'+ date.strftime("%d_%m_%Y")+'.csv' 
        if os.path.isfile(filename):
            tips_date = get_dataframe_from_database(filename)
            
            tips_date = tips_date[tips_date["Author"]==name]
            
    
            if tips is not None:
                tips = pd.concat([tips,tips_date])
            else:
                tips = tips_date
        
        date += datetime.timedelta(days=1)
        
    if last is not None:
        tips = tips.tail(last)    
     
    if tips is not None:
        tips.index = range(len(tips))
    
    if len(tips) == 0:
        tips = None
    
    
        
    return tips        
        
def evaluate_picks(beginning_date, end_date, dir_path):
    
    date = beginning_date
    
    while date <= end_date:
        print(date)
        filename = dir_path + '/Database/' + date.strftime("%d_%m_%Y")+'.csv' 
        if os.path.isfile(filename):
            tips_date = get_dataframe_from_database(filename)
            
            for i in range(len(tips_date)):
                
                tip_to_bet = tips_date.ix[i]
                
                try:
                   tip_to_bet["FTHG"] 
                   tip_to_bet["FTAG"]
                   tip_to_bet["Prediction"]
                   tip_to_bet["Odds"]
                   tip_to_bet["Wager"]
                   tip_to_bet["Return"]
                   all_values_present = True
                except:
                   all_values_present = False 
#                     
                if all_values_present and tip_to_bet["FTHG"] is not None and tip_to_bet["FTAG"] is not None\
                and tip_to_bet["Prediction"] is not None and tip_to_bet["Odds"] and tip_to_bet["Wager"] is not None:                  
                    wins,betting_type = place_bet(tip_to_bet["FTHG"],tip_to_bet["FTAG"],tip_to_bet["Odds"],tip_to_bet["Prediction"],tip_to_bet["Wager"])
#                    
                    if wins>float(tip_to_bet["Wager"]):
                        tips_date.set_value(i,"Outcome",'Win')
                    elif np.float(wins)==np.float(tip_to_bet["Wager"]):
                        tips_date.set_value(i,"Outcome",'Push')  
                    else:
                        tips_date.set_value(i,"Outcome",'Lost')   
                    print(i)     
                    tips_date.set_value(i,"Return", wins)
                    tips_date.set_value(i,"Type of Bet", betting_type)
                        
                else:
                     tips_date.set_value(i,"Outcome",'Push') 
                     tips_date.set_value(i,"Return",tip_to_bet["Wager"]) 

            write_picks_to_database(tips_date, date, dir_path)
        
        date += datetime.timedelta(days=1)        
        
def tipster_report(beginning_date, end_date, dir_path):
    
    tipster_header = ['Name','Stakes','Return','Number of Tips','Played Odds','Correct Predictions','p-Value (est)','Edge']
    tipsters = pd.DataFrame(columns=tipster_header)

    date = beginning_date
    

    while date <= end_date:
        
        filename = dir_path + '/Database/' + date.strftime("%d_%m_%Y")+'.csv' 
        if os.path.isfile(filename):
            tips_date = get_dataframe_from_database(filename)
            
            for i in range(len(tips_date)):
                
                tip_to_bet = tips_date.ix[i]
                
                if tip_to_bet["Author"] not in tipsters["Name"].tolist():

                    index_tipster = len(tipsters)                    
                    
                    tipsters.set_value(index_tipster,"Name",tip_to_bet["Author"])
                    tipsters.set_value(index_tipster,"Stakes",0)
                    tipsters.set_value(index_tipster,"Return",0)
                    tipsters.set_value(index_tipster,"Number of Tips",0)
                    tipsters.set_value(index_tipster,"Played Odds",'')
                    tipsters.set_value(index_tipster,"Betting Types",'')
                    tipsters.set_value(index_tipster,"Correct Predictions",0)
                    tipsters.set_value(index_tipster,"p-Value (est)",0)
                    tipsters.set_value(index_tipster,"Edge",0)
                    tipsters.set_value(index_tipster,"Progression",'0')
                
                try:
                   tip_to_bet["FTHG"] 
                   tip_to_bet["FTAG"]
                   tip_to_bet["Prediction"]
                   tip_to_bet["Odds"]
                   tip_to_bet["Wager"]
                   all_values_present = True
                except:
                   all_values_present = False 
#                   
                 
                if all_values_present and tip_to_bet["FTHG"] is not None and tip_to_bet["FTAG"] is not None\
                and tip_to_bet["Prediction"] is not None and tip_to_bet["Odds"] and tip_to_bet["Wager"] is not None\
                and tip_to_bet["Outcome"] is not None:
#                    
                    index =tipsters.index[tipsters['Name'] == tip_to_bet["Author"]].tolist()
                    
                    if str(tip_to_bet["Outcome"].lower().strip()) != 'push':
#                        print tipsters["Stakes"][index]                    
                        tipsters.set_value(index,"Stakes",np.float(tipsters["Stakes"][index])+np.float(tip_to_bet["Wager"]))
    #                    
                        tipsters.set_value(index,"Return",np.float(tipsters["Return"][index])-np.float(tip_to_bet["Wager"]))
                        tipsters.set_value(index,"Number of Tips",tipsters["Number of Tips"][index]+1)
    ##                    print tipster["Played Odds"]
                        tipsters.set_value(index,"Played Odds",tipsters["Played Odds"][index]+str(tip_to_bet["Odds"])+',')
                        tipsters.set_value(index,"Betting Types",tipsters["Betting Types"][index]+str(tip_to_bet["Type of Bet"])+',')
                        
    ##                    
                        wins = np.float(tip_to_bet["Return"])
                        
#                        print tipsters["Progression"][index]
#                        add = tipsters["Progression"][index]+str(tipsters["Progression"][index].split(',')[-2])+','
#                        tipsters.set_value(index,"Progression",add)

#                        tipsters.set_value(index,"Progression",str(tipsters["Progression"][index])+','+str(np.float(tipsters["Progression"][index][0].split(',')[-1])+wins-tip_to_bet["Wager"]))

    #                    
                        if wins>0:
                            tipsters.set_value(index,"Return",np.float(tipsters["Return"][index])+wins) 
                            tipsters.set_value(index,"Correct Predictions",tipsters["Correct Predictions"][index]+1)

        
        date += datetime.timedelta(days=1)      
    
    tipsters = tipsters[tipsters["Number of Tips"] !=0]
    tipsters.index = range(len(tipsters))
#    print tipsters
#    tipsters.to_csv('test.csv')
    for i in range(len(tipsters)):
#        print i
#        print tipsters.ix[i]
#        print np.float(tipsters["Correct Predictions"][i]), np.float(tipsters["Number of Tips"][i])
        
        tipsters.set_value(i,"Correct Predictions", np.float(tipsters["Correct Predictions"][i])/np.float(tipsters["Number of Tips"][i]))
        tipsters.set_value(i,"Edge", np.float(tipsters["Return"][i])/np.float(tipsters["Stakes"][i]))
        tipsters.set_value(i,"Relative Wins", np.float(tipsters["Edge"][i])*np.float(tipsters["Number of Tips"][i]))
        tipsters.set_value(i,"Name", '/u/'+tipsters["Name"][i])
        
#        print tipsters["Played Odds"][i].split(',')[:-1]
#        print [float(i) for i in tipsters["Played Odds"][i].split(',')[:-1]]
        tipsters.set_value(i,"Mean Odds", np.mean([float(j) for j in tipsters["Played Odds"][i].split(',')[:-1]]))

        betting_style = max(set(tipsters["Betting Types"][i].split(',')[:-1]),key=tipsters["Betting Types"][i].split(',')[:-1].count)
        tipsters.set_value(i,"Betting Style", betting_style)
        
        B1 = np.float(tipsters["Number of Tips"][i])
        B2 = np.float(tipsters["Relative Wins"][i])/100.0
        B3 = np.float(tipsters["Mean Odds"][i])
        
        if (B3-1-B2)>0 and B2 >0 and B1 >5:
            B4 = ((1+B2)*(B3-1-B2))**0.5
            B5 = B2*(B1**0.5)/B4
            B6 = t.sf(B5,B1-1)
            
            tipsters.set_value(i,"Standard Deviation", np.float(B4))
            tipsters.set_value(i,"t-statistic", np.float(B5))
            tipsters.set_value(i,"p-Value", np.float(B6))
        
#        print tipsters["Progression"][i]
#        tipsters.set_value(i,"Progression",np.array(tipsters["Progression"][i][:-1].split(',')).astype(np.float))
    tipsters = tipsters.sort_values(['Relative Wins'], ascending=[False])
    tipsters.index = range(len(tipsters))
    
    return tipsters
    


    
    

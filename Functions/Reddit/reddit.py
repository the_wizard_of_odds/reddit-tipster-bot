import praw
import datetime

bot = praw.Reddit('tipster_bot',user_agent='TipserReportTest v0.1')

subreddit = 'SoccerBetting'
#subreddit = 'test'


def get_picks_thread_from_date(date):       
             

    for thread in bot.subreddit(subreddit).hot(limit=15):          
#        print date.strftime("%-d/%m/%Y") in thread.title
        if "Daily Picks".lower() in  thread.title.lower() and (date.strftime("%d/%m/%Y") in thread.title or date.strftime("%d/%m/%y") in thread.title or date.strftime("%d/%m/%y") in thread.title or date.strftime("%-d/%m/%Y") in thread.title or date.strftime("%-d/%-m/%Y") in thread.title or date.strftime("%d/%-m/%Y") in thread.title):   
            return thread
     
    print("Could not find Picks Thread")
    return None       
           
def get_top_level_comments(thread):
    
    comments = []    
    
    #kick out all but top_level comments
    thread.comments.replace_more(limit=0)
    
    for top_level_comment in thread.comments:
        comments.append(top_level_comment)
        
    return comments  
    
def write_weekly_tipster_report(tipsters,begining,end):
#    
#    begining = datetime.datetime.today()-datetime.timedelta(days=7)
#    end = datetime.datetime.today()-datetime.timedelta(days=0)

#    title = 'Tipster Bot '+ begining.strftime("%B") +' report: '+ begining.strftime("%d/%m/%y") + ' - '+ end.strftime("%d/%m/%y")
    title = 'Tipster Bot '+ begining.strftime("%B") +' report: '+ 'Week '+ str(int(round(end.day/7.0)))
     
    print(title)
    tables_report_header = ['Placing','Tipster','Stakes [Units]','Wins [Units]','Tips','Prediction Rate [%]', 'ROI [%]']
    tables_report = ['Index','Name','Stakes','Return','Number of Tips','Correct Predictions', 'Edge']
    
    number_headers = ['Return','Edge','Correct Predictions']
    percent_headers = ['Edge','Correct Predictions']
    no_print_header = ["Played Odds",'p-Value (est)','Relative Wins']
    
#    text = '####This is the weekly tipster report from '+ begining.strftime("%d/%m/%y") + ' - '+ end.strftime("%d/%m/%y") +'.\n\n'
    
    text = 'In order to get some reliable information on the tipsters in the Daily Picks Thread from /r/SoccerBetting, this Bot tracks the picks and \
    writes consecutive weekly reports. At the beginning of each month they start anew. These are the results from **' + begining.strftime("%d/%m/%y") + ' to the '+end.strftime("%d/%m/%y")+'**.\n\n'    
    
    text += 'Since the beginning of '+ begining.strftime("%B")  +', we had '+str(len(tipsters))+' Tipsters and '+str(sum(tipsters["Number of Tips"]))+' Tips that were tracked by the bot. The results are presented in the table below.\n\n' 
    
    index_tipster_of_the_week = tipsters["Relative Wins"].argmax()
    
    text += 'Currently '+'**'+str(tipsters["Name"][index_tipster_of_the_week])+'** ' +'is leading the table with a ROI of '+ "{:.2f}".format(float(tipsters["Edge"][index_tipster_of_the_week])*100)+'%'+ ' over '+ str(tipsters["Number of Tips"][index_tipster_of_the_week])+ ' Picks. Congratulations! \
    The Tipsters are sorted after the highest product of ROI and Number of Tips (i.e. the Tipster that had the highest wins, if all tipsters had the same stake per game).\n\n'
    text += '\n'
    
    
    table_header = list(tipsters)   
    for j in range(len(tables_report_header)):
            text += str(tables_report_header[j])+' | '    

    text += '\n'  
    
    for j in range(len(tables_report_header)):
        text += ':-:'+' | ' 
        
    text += '\n'   

    for i in range(len(tipsters)):
        for j in range(len(tables_report)):
                
                if tables_report[j] is not 'Index':
            
                    index = list(tipsters).index(tables_report[j])
                    if tables_report[j] in number_headers:
                        if tables_report[j] in percent_headers: 
                            text += "{:.2f}%".format(100*float(tipsters.ix[i].tolist()[index]))+' | ' 
                        else:
                            text += "{:.3f}".format(float(tipsters.ix[i].tolist()[index]))+' | ' 
                    else:
                        text += str(tipsters.ix[i].tolist()[index])+' | ' 
                
                else:
                    if i+1 > 3:
                        text += str(i+1)+'. | ' 
                    else:
                        text += '**'+str(i+1)+'.** | ' 
            
        text += '\n'
            
    text += 'Thanks to all participants. Please take into account that the bot does not track all tipsters. If you are interested in having your tips logged next week, I made an in-depth [post](https://www.reddit.com/user/tropianhs/comments/7o4o4o/how_do_i_get_tracked_by_the_tipster_bot/) how to format your tips in the Daily Picks Thread. Please be advised that the weekly report only contains a small sample size. For data tracked over a longer time, take a look at this [post](https://www.reddit.com/user/tropianhs/comments/7o4o4o/how_do_i_get_tracked_by_the_tipster_bot/)\n\n'     
    text += 'Until next week, good luck to all and happy betting. \n\n\n'     
    
    text += ''
    
    text += '\n\n---\n\n'  
    text += '^(This post was created automatically by a scripting bot. If you find a mistake, just PM me. If you would like to add a feature to the bot, we can discuss it in the comments.)\n\n'
    #text += '^().)'
    
    print(text)              
    #bot.subreddit(subreddit).submit(title, selftext=text)    
    
def get_messages():    
    
#    bot.inbox
    messages = []
    authors = []
    for message in bot.inbox.messages(limit=10):
        if '!Picks TipsterBot'.lower().strip() in str(message.subject).lower().strip() and len(message.replies) == 0:
            messages.append(message)
            authors.append(str(message.author.name))
        
    return messages,authors
    
#def answer_messages(message,text,author):
#
##    bot.send_message(author,'Your tips', message)
#    message.reply(text)
##    bot.redditor(author).message('Your tips', message)
        
    
def reply_with_tips(tips):
    
    if tips is not None:
    
        #print("Return: ", sum(tips["Return"])-sum(tips["Wager"]))
    
        no_print_header = ['Author','Fetched at','Post Time']
        
        text = 'These are the picks I have in my database: \n\n'
        table_header = list(tips)   
        for j in range(len(table_header)):
                    if table_header[j] not in no_print_header:     
                        text += str(table_header[j])+' | '    
        
        text += ' \n '  
        
        for j in range(len(table_header)-len(no_print_header)):
            text += ':-:'+' | ' 
                
        text += ' \n '   
        
        for i in range(len(tips)):
            for j in range(len(table_header)):
                if table_header[j] not in no_print_header:
                    text+=str(tips.ix[i][table_header[j]])+' | ' 
            text += ' \n '
    
    else:
        text = 'Sorry, I could not find any picks for you in the database'        
        
    return text   
    
def write_monthly_tipster_report(tipsters,begining,end):
#    
#    begining = datetime.datetime.today()-datetime.timedelta(days=7)
#    end = datetime.datetime.today()-datetime.timedelta(days=0)

#    title = 'Tipster Bot '+ begining.strftime("%B") +' report: '+ begining.strftime("%d/%m/%y") + ' - '+ end.strftime("%d/%m/%y")
    title = 'Tipster Bot '+ begining.strftime("%B") +' report: '+ 'Week '+ str(int(round(end.day/7.0)))
     
    print(title)
    tables_report_header = ['Placing','Tipster','Stakes [Units]','Wins [Units]','Tips','Prediction Rate [%]', 'ROI [%]','Mean Odds', 'Main Betting Style']
    tables_report = ['Index','Name','Stakes','Return','Number of Tips','Correct Predictions', 'Edge','Mean Odds','Betting Style']
    
    number_headers = ['Return','Edge','Correct Predictions','Mean Odds']
    percent_headers = ['Edge','Correct Predictions']
    no_print_header = ["Played Odds",'p-Value (est)','Relative Wins']
    
#    text = '####This is the weekly tipster report from '+ begining.strftime("%d/%m/%y") + ' - '+ end.strftime("%d/%m/%y") +'.\n\n'
    
    text = 'In order to get some reliable information on the tipsters in the Daily Picks Thread from /r/SoccerBetting, this Bot tracks the picks and \
    writes consecutive weekly reports. At the beginning of each month they start anew. These are the results from **' + begining.strftime("%d/%m/%y") + ' to the '+end.strftime("%d/%m/%y")+'**.\n\n'    
    
    text += 'Since the beginning of '+ begining.strftime("%B")  +', we had '+str(len(tipsters))+' Tipsters and '+str(sum(tipsters["Number of Tips"]))+' Tips that were tracked by the bot. The results are presented in the table below.\n\n' 
    
    index_tipster_of_the_week = tipsters["Relative Wins"].argmax()
    
    text += 'The **Tipster of the Month** is '+'**'+str(tipsters["Name"][index_tipster_of_the_week])+'** ' +' with a ROI of '+ "{:.2f}".format(float(tipsters["Edge"][index_tipster_of_the_week])*100)+'%'+ ' over '+ str(tipsters["Number of Tips"][index_tipster_of_the_week])+ ' Picks. Congratulations! \
    The Tipsters are sorted after the highest product of ROI and Number of Tips (i.e. the Tipster that had the highest wins, if all tipsters had the same stake per game).\n\n'
    text += '\n'
    
    
    table_header = list(tipsters)   
    for j in range(len(tables_report_header)):
            text += str(tables_report_header[j])+' | '    

    text += '\n'  
    
    for j in range(len(tables_report_header)):
        text += ':-:'+' | ' 
        
    text += '\n'   

    for i in range(len(tipsters)):
        for j in range(len(tables_report)):
                
                if tables_report[j] is not 'Index':
            
                    index = list(tipsters).index(tables_report[j])
                    if tables_report[j] in number_headers:
                        if tables_report[j] in percent_headers: 
                            text += "{:.2f}%".format(100*float(tipsters.ix[i].tolist()[index]))+' | ' 
                        else:
                            text += "{:.3f}".format(float(tipsters.ix[i].tolist()[index]))+' | ' 
                    else:
                        text += str(tipsters.ix[i].tolist()[index])+' | ' 
                
                else:
                    if i+1 > 3:
                        text += str(i+1)+'. | ' 
                    else:
                        text += '**'+str(i+1)+'.** | ' 
            
        text += '\n'
            
    text += 'Thanks to all participants. Please take into account that the bot does not track all tipsters. If you are interested in having your tips logged next week, I made an in-depth [post](https://www.reddit.com/user/tropianhs/comments/7o4o4o/how_do_i_get_tracked_by_the_tipster_bot/) how to format your tips in the Daily Picks Thread.\n\n'     
    text += 'Until next week, good luck to all and happy betting. \n\n\n'     
    
    text += ''
    
    text += '\n\n---\n\n'  
    text += '^(This post was created automatically by a scripting bot. If you find a mistake, just PM me. If you would like to add a feature to the bot, we can discuss it in the comments.)\n\n'
#    text += '^(If you want your picks to get tracked, take a look at this [post](https://www.reddit.com/user/the_wizard_of_odds/comments/6yh8w7/how_do_i_get_tracked_by_the_tipster_bot/).)'
    
    print(text)              
    #bot.subreddit(subreddit).submit(title, selftext=text) 

def write_in_depth_tipster(tipsters,begining,end):
#    
#    begining = datetime.datetime.today()-datetime.timedelta(days=7)
#    end = datetime.datetime.today()-datetime.timedelta(days=0)

#    title = 'Tipster Bot '+ begining.strftime("%B") +' report: '+ begining.strftime("%d/%m/%y") + ' - '+ end.strftime("%d/%m/%y")
    title = 'Tipster Bot '+ begining.strftime("%B") +' report: '+ 'Week '+ str(int(round(end.day/7.0)))
     
    print(title)
    tables_report_header = ['Placing','Tipster','Stakes [Units]','Wins [Units]','Tips','Prediction Rate [%]', 'ROI [%]','Mean Odds', 'Main Betting Style']
    tables_report = ['Index','Name','Stakes','Return','Number of Tips','Correct Predictions', 'Edge','Mean Odds','Betting Style']
    
    p_header = ['Placing','Tipster','Stakes [Units]','Wins [Units]','Tips','Mean Odds','Standard Deviation','t-statistic','p-Value']
    p = ['Index','Name','Stakes','Return','Number of Tips','Mean Odds','Standard Deviation','t-statistic','p-Value']
        
    
    number_headers = ['Return','Edge','Correct Predictions','Mean Odds','Standard Deviation','t-statistic','p-Value']
    percent_headers = ['Edge','Correct Predictions']
    no_print_header = ["Played Odds",'p-Value (est)','Relative Wins']
    
#    text = '####This is the weekly tipster report from '+ begining.strftime("%d/%m/%y") + ' - '+ end.strftime("%d/%m/%y") +'.\n\n'
    
    text = 'These are the results from **' + begining.strftime("%d/%m/%y") + ' to the '+end.strftime("%d/%m/%y")+'**.\n\n'    
    
    text += 'Since the beginning of '+ begining.strftime("%B")  +', we had '+str(len(tipsters))+' Tipsters and '+str(sum(tipsters["Number of Tips"]))+' Tips that were tracked by the bot. The results are presented in the table below.\n\n' 
    
    index_tipster_of_the_week = tipsters["Relative Wins"].argmax()
    
#    text += 'The **Tipster of the Month** is '+'**'+str(tipsters["Name"][index_tipster_of_the_week])+'** ' +' with a ROI of '+ "{:.2f}".format(float(tipsters["Edge"][index_tipster_of_the_week])*100)+'%'+ ' over '+ str(tipsters["Number of Tips"][index_tipster_of_the_week])+ ' Picks. Congratulations! \
#    The Tipsters are sorted after the highest product of ROI and Number of Tips (i.e. the Tipster that had the highest wins, if all tipsters had the same stake per game).\n\n'
#    text += '\n'
    
    
    table_header = list(tipsters)   
    for j in range(len(tables_report_header)):
            text += str(tables_report_header[j])+' | '    

    text += '\n'  
    
    for j in range(len(tables_report_header)):
        text += ':-:'+' | ' 
        
    text += '\n'   

    for i in range(len(tipsters)):
        for j in range(len(tables_report)):
                
                if tables_report[j] is not 'Index':
            
                    index = list(tipsters).index(tables_report[j])
                    if tables_report[j] in number_headers:
                        if tables_report[j] in percent_headers: 
                            text += "{:.2f}%".format(100*float(tipsters.ix[i].tolist()[index]))+' | ' 
                        else:
                            text += "{:.3f}".format(float(tipsters.ix[i].tolist()[index]))+' | ' 
                    else:
                        text += str(tipsters.ix[i].tolist()[index])+' | ' 
                
                else:
                    if i+1 > 3:
                        text += str(i+1)+'. | ' 
                    else:
                        text += '**'+str(i+1)+'.** | ' 
            
        text += '\n'
            
    text += 'p values..., for everyone with more than 5 games and positive ROI'     
    
    tipsters = tipsters[tipsters["p-Value"]<0.5][tipsters["Number of Tips"]>10]
    tipsters = tipsters.sort_values(['p-Value'], ascending=[True])
    tipsters.index = range(len(tipsters))
    
    text += '\n'  
    
    table_header = list(tipsters)   
    for j in range(len(p_header)):
            text += str(p_header[j])+' | '    

    text += '\n'  
    
    for j in range(len(p_header)):
        text += ':-:'+' | '
        
    text += '\n'      
    
    for i in range(len(tipsters)):
        for j in range(len(p)):
                
                if p[j] is not 'Index':
            
                    index = list(tipsters).index(p[j])
                    if p[j] in number_headers:
                        if p[j] in percent_headers: 
                            text += "{:.2f}%".format(100*float(tipsters.ix[i].tolist()[index]))+' | ' 
                        else:
                            text += "{:.3f}".format(float(tipsters.ix[i].tolist()[index]))+' | ' 
                    else:
                        
                        text += str(tipsters.ix[i].tolist()[index])+' | ' 
                
                else:
                    if i+1 > 3:
                        text += str(i+1)+'. | ' 
                    else:
                        text += '**'+str(i+1)+'.** | ' 
            
        text += '\n'
    
    
    print(text)                      

# -*- coding: utf-8 -*-
from Functions.Reddit.reddit import get_messages
from Functions.Database.database import get_tips_by_name
from Functions.Reddit.reddit import reply_with_tips

import datetime 

beginning = datetime.date(2016,9,1)
ending = datetime.date.today()+datetime.timedelta(days=1)

messages,authors = get_messages()
texts = []

for i in range(len(authors)):
    tips = get_tips_by_name(beginning,ending,authors[i],last=80)
    
    text = reply_with_tips(tips)
    print text
    
    messages[i].reply(text)

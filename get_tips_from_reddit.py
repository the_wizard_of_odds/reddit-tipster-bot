# -*+*- coding: utf-8 -*-

import Functions.Reddit.reddit as reddit_bot
from Functions.Tips.tips import extract_tips_from_comment, merge_tips_to_dataframe
from Functions.Database.database import write_picks_to_database
import os
import datetime 

date = datetime.date.today()

dir_path = os.path.dirname(os.path.realpath(__file__))

while date < datetime.date.today()+datetime.timedelta(days=2):
    
    print("Scraping Picks from: ", date.strftime("%d_%m_%Y"))

    thread = reddit_bot.get_picks_thread_from_date(date)  
    
    if thread is not None:
    
        comments = reddit_bot.get_top_level_comments(thread)   
        
        tips =[]
        authors = []
        
        for i in range(len(comments)):
            
            author_comment,tips_comment = extract_tips_from_comment(comments[i])   
            
            if tips_comment is not None:
                authors.append(author_comment)
                tips.append(tips_comment)
                
        if len(tips)>0:        
            tips = merge_tips_to_dataframe(tips)    
            tips = write_picks_to_database(tips,date, dir_path)

    print tips
            
    date = date+datetime.timedelta(days=1)    

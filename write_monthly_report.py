from Functions.Database.database import write_picks_to_database, evaluate_picks, tipster_report
from Functions.Reddit.reddit import write_monthly_tipster_report
import datetime 
import os 

dir_path = os.path.dirname(os.path.realpath(__file__))

# begining = datetime.date.today()-datetime.timedelta(days=7)
# begining = datetime.date.today().replace(day=1)
# ending = datetime.date.today()-datetime.timedelta(days=1)
beginning = datetime.date(2019, 01, 1)
ending = datetime.date(2019, 01, 31)

evaluate_picks(beginning, ending, dir_path)

tipsters = tipster_report(beginning, ending, dir_path)

write_monthly_tipster_report(tipsters, beginning, ending)

from Functions.Database.database import write_picks_to_database,evaluate_picks,tipster_report
from Functions.Reddit.reddit import write_weekly_tipster_report
import os
import datetime 

dir_path = os.path.dirname(os.path.realpath(__file__))

#begining = datetime.date.today()-datetime.timedelta(days=7)
begining = datetime.date.today().replace(day=1)
ending = datetime.date.today()-datetime.timedelta(days=4)

evaluate_picks(begining,ending, dir_path)

tipsters = tipster_report(begining,ending,dir_path)

write_weekly_tipster_report(tipsters,begining,ending)

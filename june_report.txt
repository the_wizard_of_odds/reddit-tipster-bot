2018-06-01
2018-06-02
2018-06-03
2018-06-04
2018-06-05
2018-06-06
2018-06-07
2018-06-08
2018-06-09
2018-06-10
2018-06-11
2018-06-12
2018-06-13
2018-06-14
2018-06-15
2018-06-16
2018-06-17
2018-06-18
2018-06-19
2018-06-20
2018-06-21
2018-06-22
2018-06-23
2018-06-24
2018-06-25
2018-06-26
2018-06-27
2018-06-28
2018-06-29
2018-06-30
Tipster Bot June report: Week 4
In order to get some reliable information on the tipsters in the Daily Picks Thread from /r/SoccerBetting, this Bot tracks the picks and     writes consecutive weekly reports. At the beginning of each month they start anew. These are the results from **01/06/18 to the 30/06/18**.

Since the beginning of June, we had 3 Tipsters and 153 Tips that were tracked by the bot. The results are presented in the table below.

The **Tipster of the Month** is **/u/ExperientialAra**  with a ROI of 1.75% over 95 Picks. Congratulations!     The Tipsters are sorted after the highest product of ROI and Number of Tips (i.e. the Tipster that had the highest wins, if all tipsters had the same stake per game).


Placing | Tipster | Stakes [Units] | Wins [Units] | Tips | Prediction Rate [%] | ROI [%] | Mean Odds | Main Betting Style | 
:-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | 
**1.** | /u/ExperientialAra | 331.0 | 5.800 | 95 | 64.21% | 1.75% | 1.647 | 1X2 | 
**2.** | /u/coolcoconut123 | 103.0 | 2.660 | 52 | 44.23% | 2.58% | 2.209 | Over | 
**3.** | /u/Nyrociel | 8.5 | -2.875 | 6 | 33.33% | -33.82% | 2.183 | 1X2 | 
Thanks to all participants. Please take into account that the bot does not track all tipsters. If you are interested in having your tips logged next week, I made an in-depth [post](https://www.reddit.com/user/tropianhs/comments/7o4o4o/how_do_i_get_tracked_by_the_tipster_bot/) how to format your tips in the Daily Picks Thread.

Until next week, good luck to all and happy betting. 




---

^(This post was created automatically by a scripting bot. If you find a mistake, just PM me. If you would like to add a feature to the bot, we can discuss it in the comments.)


